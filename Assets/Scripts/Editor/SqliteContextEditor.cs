﻿using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;

namespace MQB.Model
{
    [CustomEditor(typeof(SqliteContext))]
    [CanEditMultipleObjects]
    public class SqliteContextEditor : Editor
    {
        // -------------------
        // Public definitions.
        // -------------------

        public override void OnInspectorGUI()
        {
            GearTypeTable_Show();
            GearTable_Show();
            GearByStarsTable_Show();
            SecondaryStatTable_Show();
            TierTable_Show();
            GearByGearTypeTable_Show();
            SecondaryStatByTierTable_Show();
            SecondaryStatByGearTable_Show();
        }

        // -------
        // Tables.
        // -------

        #region Utils

        private struct DropDownList
        {
            public List<int> field_id;
            public List<GUIContent> field_name;

            // -------------------
            // Public definitions.
            // -------------------

            public static DropDownList Create(List<POCO.Gear> gears)
            {
                DropDownList drop_down_list = new DropDownList
                {
                    field_id = new List<int>(),
                    field_name = new List<GUIContent>()
                };

                foreach (POCO.Gear gear in gears)
                {
                    drop_down_list.field_id.Add(gear.id);
                    drop_down_list.field_name.Add(new GUIContent(gear.name));
                }

                return drop_down_list;
            }

            public static DropDownList Create(List<POCO.GearType> gear_types)
            {
                DropDownList drop_down_list = new DropDownList
                {
                    field_id = new List<int>(),
                    field_name = new List<GUIContent>()
                };

                foreach (POCO.GearType gear_type in gear_types)
                {
                    drop_down_list.field_id.Add(gear_type.id);
                    drop_down_list.field_name.Add(new GUIContent(gear_type.name));
                }

                return drop_down_list;
            }

            public static DropDownList Create(List<POCO.SecondaryStat> secondary_stats)
            {
                DropDownList drop_down_list = new DropDownList
                {
                    field_id = new List<int>(),
                    field_name = new List<GUIContent>()
                };

                foreach (POCO.SecondaryStat secondary_stat in secondary_stats)
                {
                    drop_down_list.field_id.Add(secondary_stat.id);
                    drop_down_list.field_name.Add(new GUIContent(secondary_stat.name));
                }

                return drop_down_list;
            }

            public static DropDownList Create(List<POCO.Tier> tiers)
            {
                DropDownList drop_down_list = new DropDownList
                {
                    field_id = new List<int>(),
                    field_name = new List<GUIContent>()
                };

                foreach (POCO.Tier tier in tiers)
                {
                    drop_down_list.field_id.Add(tier.id);
                    drop_down_list.field_name.Add(new GUIContent(tier.name));
                }

                return drop_down_list;
            }

            public int GetDropdownID(int field_id)
            {
                for (int i = 0; i < this.field_id.Count; i++)
                {
                    if (this.field_id[i] == field_id)
                        return i;
                }

                return 0;
            }

            public int GetFieldId(int dropdown_id)
            {
                return this.field_id[dropdown_id];
            }
        }

        #endregion

        #region GearType Table

        // -----------------
        // Internal members.
        // -----------------

        private bool __gear_type_foldout = false;
        private List<POCO.GearType> __gear_type_table = null;

        // ---------------------
        // Internal definitions.
        // ---------------------

        private void GearTypeTable_Show()
        {
            __gear_type_foldout = EditorGUILayout.Foldout(__gear_type_foldout, "Gear Types");

            if (__gear_type_foldout)
            {
                GearTypeTable_Initialize();

                GearTypeTable_ShowData();

                GearTypeTable_ShowControls();
            }
        }

        private void GearTypeTable_Initialize()
        {
            if (__gear_type_table == null)
                __gear_type_table = DAL.GearTypeTable.GetField();
        }

        private void GearTypeTable_ShowData()
        {
            for (int i = 0; i < __gear_type_table.Count; i++)
            {
                EditorGUILayout.BeginHorizontal();

                EditorGUIUtility.labelWidth = GUI.skin.label.CalcSize(new GUIContent("Id:")).x;
                EditorGUILayout.LabelField("Id:", "" + __gear_type_table[i].id, new GUILayoutOption[] { GUILayout.ExpandWidth(false) });

                EditorGUIUtility.labelWidth = GUI.skin.label.CalcSize(new GUIContent("Name:")).x;
                __gear_type_table[i].name = EditorGUILayout.TextField("Name:", __gear_type_table[i].name);

                EditorGUILayout.EndHorizontal();
            }
        }

        private void GearTypeTable_ShowControls()
        {
            EditorGUILayout.BeginHorizontal();

            if (GUILayout.Button("Add Field", GUILayout.ExpandWidth(false)))
            {
                GearTypeTable_AddField();
            }

            if (GUILayout.Button("Clear Table", GUILayout.ExpandWidth(false)))
            {
                GearTypeTable_ClearTable();
            }

            if (GUILayout.Button("Save Table", GUILayout.ExpandWidth(false)))
            {
                GearTypeTable_SaveTable();
            }

            EditorGUILayout.EndHorizontal();
        }

        private void GearTypeTable_AddField()
        {
            DAL.GearTypeTable.AddField("");

            __gear_type_table = DAL.GearTypeTable.GetField();
        }

        private void GearTypeTable_SaveTable()
        {
            foreach (POCO.GearType gear_type in __gear_type_table)
            {
                DAL.GearTypeTable.SetField(gear_type);
            }

            __gear_type_table = DAL.GearTypeTable.GetField();
        }

        private void GearTypeTable_ClearTable()
        {
            DAL.GearTypeTable.RemoveField();

            __gear_type_table = DAL.GearTypeTable.GetField();
        }

        #endregion

        #region Gear Table

        // -----------------
        // Internal members.
        // -----------------

        private bool __gear_foldout = false;
        private List<POCO.Gear> __gear_table = null;

        // ---------------------
        // Internal definitions.
        // ---------------------

        private void GearTable_Show()
        {
            __gear_foldout = EditorGUILayout.Foldout(__gear_foldout, "Gears");

            if (__gear_foldout)
            {
                GearTable_Initialize();

                GearTable_ShowData();

                GearTable_ShowControls();
            }
        }

        private void GearTable_Initialize()
        {
            if (__gear_table == null)
                __gear_table = DAL.GearTable.GetField();
        }

        private void GearTable_ShowData()
        {
            for (int i = 0; i < __gear_table.Count; i++)
            {
                EditorGUILayout.BeginHorizontal();

                EditorGUIUtility.labelWidth = GUI.skin.label.CalcSize(new GUIContent("id:")).x;
                EditorGUILayout.LabelField("Id:", "" + __gear_table[i].id);

                EditorGUIUtility.labelWidth = GUI.skin.label.CalcSize(new GUIContent("name:")).x;
                __gear_table[i].name = EditorGUILayout.TextField("Name:", __gear_table[i].name);

                EditorGUILayout.EndHorizontal();
            }
        }

        private void GearTable_ShowControls()
        {
            EditorGUILayout.BeginHorizontal();

            if (GUILayout.Button("Add Field", GUILayout.ExpandWidth(false)))
            {
                GearTable_AddField();
            }

            if (GUILayout.Button("Clear Table", GUILayout.ExpandWidth(false)))
            {
                GearTable_ClearTable();
            }

            if (GUILayout.Button("Save Table", GUILayout.ExpandWidth(false)))
            {
                GearTable_SaveTable();
            }

            EditorGUILayout.EndHorizontal();
        }

        private void GearTable_AddField()
        {
            DAL.GearTable.AddField("");

            __gear_table = DAL.GearTable.GetField();
        }

        private void GearTable_SaveTable()
        {
            foreach (POCO.Gear gear in __gear_table)
            {
                DAL.GearTable.SetField(gear);
            }

            __gear_table = DAL.GearTable.GetField();
        }

        private void GearTable_ClearTable()
        {
            DAL.GearTable.RemoveField();

            __gear_table = DAL.GearTable.GetField();
        }

        #endregion

        #region GearByGearType Table

        // -----------------
        // Internal members.
        // -----------------

        private bool __gear_by_gear_type_foldout = false;
        private List<POCO.GearByGearType> __gear_by_gear_type_table = null;

        // ---------------------
        // Internal definitions.
        // ---------------------

        private void GearByGearTypeTable_Show()
        {
            __gear_by_gear_type_foldout = EditorGUILayout.Foldout(__gear_by_gear_type_foldout, "Gear - Gear Type List");

            if (__gear_by_gear_type_foldout)
            {
                GearByGearTypeTable_Initialize();

                GearByGearTypeTable_ShowData();

                GearByGearTypeTable_ShowControls();
            }
        }

        private void GearByGearTypeTable_Initialize()
        {
            if (__gear_by_gear_type_table == null)
                __gear_by_gear_type_table = DAL.GearByGearTypeTable.GetField();
        }

        private void GearByGearTypeTable_ShowData()
        {
            GUIContent gear_id_ui = new GUIContent("gear_id:");
            GUIContent gear_type_id_ui = new GUIContent("gear_type_id:");

            float gear_id_ui_length = GUI.skin.label.CalcSize(gear_id_ui).x;
            float gear_type_id_ui_length = GUI.skin.label.CalcSize(gear_type_id_ui).x;

            DropDownList gear_dropdown = DropDownList.Create(DAL.GearTable.GetField());
            DropDownList gear_type_dropown = DropDownList.Create(DAL.GearTypeTable.GetField());

            GUIContent[] gear_dropdown_names = gear_dropdown.field_name.ToArray();
            GUIContent[] gear_type_dropdown_names = gear_type_dropown.field_name.ToArray();

            for (int i = 0; i < __gear_by_gear_type_table.Count; i++)
            {
                EditorGUILayout.BeginHorizontal();

                EditorGUIUtility.labelWidth = gear_id_ui_length;
                __gear_by_gear_type_table[i].gear_id = gear_dropdown.GetFieldId(
                    EditorGUILayout.Popup(
                        gear_id_ui, 
                        gear_dropdown.GetDropdownID(__gear_by_gear_type_table[i].gear_id),
                        gear_dropdown_names));

                EditorGUIUtility.labelWidth = gear_type_id_ui_length;
                __gear_by_gear_type_table[i].gear_type_id = gear_type_dropown.GetFieldId(
                    EditorGUILayout.Popup(
                        gear_type_id_ui, 
                        gear_type_dropown.GetDropdownID(__gear_by_gear_type_table[i].gear_type_id),
                        gear_type_dropdown_names));

                EditorGUILayout.EndHorizontal();
            }
        }

        private void GearByGearTypeTable_ShowControls()
        {
            EditorGUILayout.BeginHorizontal();

            if (GUILayout.Button("Add Field", GUILayout.ExpandWidth(false)))
            {
                GearByGearTypeTable_AddField();
            }

            if (GUILayout.Button("Clear Table", GUILayout.ExpandWidth(false)))
            {
                GearByGearTypeTable_ClearTable();
            }

            if (GUILayout.Button("Save Table", GUILayout.ExpandWidth(false)))
            {
                GearByGearTypeTable_SaveTable();
            }

            EditorGUILayout.EndHorizontal();
        }

        private void GearByGearTypeTable_AddField()
        {
            DAL.GearByGearTypeTable.AddField(0, 0);

            __gear_by_gear_type_table = DAL.GearByGearTypeTable.GetField();
        }

        private void GearByGearTypeTable_SaveTable()
        {
            foreach (POCO.GearByGearType gear_by_gear_type in __gear_by_gear_type_table)
            {
                DAL.GearByGearTypeTable.SetField(gear_by_gear_type);
            }

            __gear_by_gear_type_table = DAL.GearByGearTypeTable.GetField();
        }

        private void GearByGearTypeTable_ClearTable()
        {
            DAL.GearByGearTypeTable.RemoveField();

            __gear_by_gear_type_table = DAL.GearByGearTypeTable.GetField();
        }

        #endregion

        #region GearByStars Table

        // -----------------
        // Internal members.
        // -----------------

        private bool __gear_by_stars_foldout = false;
        private List<POCO.GearByStars> __gear_by_stars_table = null;

        // ---------------------
        // Internal definitions.
        // ---------------------

        private void GearByStarsTable_Show()
        {
            __gear_by_stars_foldout = EditorGUILayout.Foldout(__gear_by_stars_foldout, "Gear - Stars List");

            if (__gear_by_stars_foldout)
            {
                GearByStarsTable_Initialize();

                GearByStarsTable_ShowData();

                GearByStarsTable_ShowControls();
            }
        }

        private void GearByStarsTable_Initialize()
        {
            if (__gear_by_stars_table == null)
                __gear_by_stars_table = DAL.GearByStarsTable.GetField();
        }

        private void GearByStarsTable_ShowData()
        {
            GUIContent gear_id_ui = new GUIContent("gear_id:");
            GUIContent stars_ui = new GUIContent("stars:");

            float gear_id_ui_length = GUI.skin.label.CalcSize(gear_id_ui).x;
            float stars_ui_length = GUI.skin.label.CalcSize(stars_ui).x;

            DropDownList gear_dropdown = DropDownList.Create(DAL.GearTable.GetField());

            GUIContent[] gear_dropdown_names = gear_dropdown.field_name.ToArray();

            for (int i = 0; i < __gear_by_stars_table.Count; i++)
            {
                EditorGUILayout.BeginHorizontal();

                EditorGUIUtility.labelWidth = gear_id_ui_length;
                __gear_by_stars_table[i].gear_id = gear_dropdown.GetFieldId(
                    EditorGUILayout.Popup(
                        gear_id_ui,
                        gear_dropdown.GetDropdownID(__gear_by_stars_table[i].gear_id),
                        gear_dropdown_names));

                EditorGUIUtility.labelWidth = stars_ui_length;
                __gear_by_stars_table[i].stars = EditorGUILayout.IntField(
                    stars_ui,
                    __gear_by_stars_table[i].stars);

                EditorGUILayout.EndHorizontal();
            }
        }

        private void GearByStarsTable_ShowControls()
        {
            EditorGUILayout.BeginHorizontal();

            if (GUILayout.Button("Add Field", GUILayout.ExpandWidth(false)))
            {
                GearByStarsTable_AddField();
            }

            if (GUILayout.Button("Clear Table", GUILayout.ExpandWidth(false)))
            {
                GearByStarsTable_ClearTable();
            }

            if (GUILayout.Button("Save Table", GUILayout.ExpandWidth(false)))
            {
                GearByStarsTable_SaveTable();
            }

            EditorGUILayout.EndHorizontal();
        }

        private void GearByStarsTable_AddField()
        {
            DAL.GearByStarsTable.AddField(0, 0);

            __gear_by_stars_table = DAL.GearByStarsTable.GetField();
        }

        private void GearByStarsTable_SaveTable()
        {
            foreach (POCO.GearByStars gear_by_stars in __gear_by_stars_table)
            {
                DAL.GearByStarsTable.SetField(gear_by_stars);
            }

            __gear_by_stars_table = DAL.GearByStarsTable.GetField();
        }

        private void GearByStarsTable_ClearTable()
        {
            DAL.GearByStarsTable.RemoveField();

            __gear_by_stars_table = DAL.GearByStarsTable.GetField();
        }

        #endregion

        #region SecondaryStat Table

        // -----------------
        // Internal members.
        // -----------------

        private bool __secondary_stat_foldout = false;
        private List<POCO.SecondaryStat> __secondary_stat_table = null;

        // ---------------------
        // Internal definitions.
        // ---------------------

        private void SecondaryStatTable_Show()
        {
            __secondary_stat_foldout = EditorGUILayout.Foldout(__secondary_stat_foldout, "Secondary Stats");

            if (__secondary_stat_foldout)
            {
                SecondaryStatTable_Initialize();

                SecondaryStatTable_ShowData();

                SecondaryStatTable_ShowControls();
            }
        }

        private void SecondaryStatTable_Initialize()
        {
            if (__secondary_stat_table == null)
                __secondary_stat_table = DAL.SecondaryStatTable.GetField();
        }

        private void SecondaryStatTable_ShowData()
        {
            for (int i = 0; i < __secondary_stat_table.Count; i++)
            {
                EditorGUILayout.BeginHorizontal();

                EditorGUIUtility.labelWidth = GUI.skin.label.CalcSize(new GUIContent("id:")).x;
                EditorGUILayout.LabelField("Id:", "" + __secondary_stat_table[i].id);

                EditorGUIUtility.labelWidth = GUI.skin.label.CalcSize(new GUIContent("name:")).x;
                __secondary_stat_table[i].name = EditorGUILayout.TextField("Name:", __secondary_stat_table[i].name);

                EditorGUILayout.EndHorizontal();
            }
        }

        private void SecondaryStatTable_ShowControls()
        {
            EditorGUILayout.BeginHorizontal();

            if (GUILayout.Button("Add Field", GUILayout.ExpandWidth(false)))
            {
                SecondaryStatTable_AddField();
            }

            if (GUILayout.Button("Clear Table", GUILayout.ExpandWidth(false)))
            {
                SecondaryStatTable_ClearTable();
            }

            if (GUILayout.Button("Save Table", GUILayout.ExpandWidth(false)))
            {
                SecondaryStatTable_SaveTable();
            }

            EditorGUILayout.EndHorizontal();
        }

        private void SecondaryStatTable_AddField()
        {
            DAL.SecondaryStatTable.AddField("");

            __secondary_stat_table = DAL.SecondaryStatTable.GetField();
        }

        private void SecondaryStatTable_SaveTable()
        {
            foreach (POCO.SecondaryStat secondary_stat in __secondary_stat_table)
            {
                DAL.SecondaryStatTable.SetField(secondary_stat);
            }

            __secondary_stat_table = DAL.SecondaryStatTable.GetField();
        }

        private void SecondaryStatTable_ClearTable()
        {
            DAL.SecondaryStatTable.RemoveField();

            __secondary_stat_table = DAL.SecondaryStatTable.GetField();
        }

        #endregion

        #region Tier Table

        // -----------------
        // Internal members.
        // -----------------

        private bool __tier_foldout = false;
        private List<POCO.Tier> __tier_table = null;

        // ---------------------
        // Internal definitions.
        // ---------------------

        private void TierTable_Show()
        {
            __tier_foldout = EditorGUILayout.Foldout(__tier_foldout, "Tiers");

            if (__tier_foldout)
            {
                TierTable_Initialize();

                TierTable_ShowData();

                TierTable_ShowControls();
            }
        }

        private void TierTable_Initialize()
        {
            if (__tier_table == null)
                __tier_table = DAL.TierTable.GetField();
        }

        private void TierTable_ShowData()
        {
            for (int i = 0; i < __tier_table.Count; i++)
            {
                EditorGUILayout.BeginHorizontal();

                EditorGUIUtility.labelWidth = GUI.skin.label.CalcSize(new GUIContent("id:")).x;
                EditorGUILayout.LabelField("Id:", "" + __tier_table[i].id);

                EditorGUIUtility.labelWidth = GUI.skin.label.CalcSize(new GUIContent("name:")).x;
                __tier_table[i].name = EditorGUILayout.TextField("Name:", __tier_table[i].name);

                EditorGUILayout.EndHorizontal();
            }
        }

        private void TierTable_ShowControls()
        {
            EditorGUILayout.BeginHorizontal();

            if (GUILayout.Button("Add Field", GUILayout.ExpandWidth(false)))
            {
                TierTable_AddField();
            }

            if (GUILayout.Button("Clear Table", GUILayout.ExpandWidth(false)))
            {
                TierTable_ClearTable();
            }

            if (GUILayout.Button("Save Table", GUILayout.ExpandWidth(false)))
            {
                TierTable_SaveTable();
            }

            EditorGUILayout.EndHorizontal();
        }

        private void TierTable_AddField()
        {
            DAL.TierTable.AddField("");

            __tier_table = DAL.TierTable.GetField();
        }

        private void TierTable_SaveTable()
        {
            foreach (POCO.Tier tier in __tier_table)
            {
                DAL.TierTable.SetField(tier);
            }

            __tier_table = DAL.TierTable.GetField();
        }

        private void TierTable_ClearTable()
        {
            DAL.TierTable.RemoveField();

            __tier_table = DAL.TierTable.GetField();
        }

        #endregion

        #region SecondaryStatByTier Table

        // -----------------
        // Internal members.
        // -----------------

        private bool __secondary_stat_by_tier__foldout = false;
        private int __secondary_stat_by_tier__secondary_stat_id = 1;
        private List<POCO.SecondaryStatByTier> __secondary_stat_by_tier_table = null;

        // ---------------------
        // Internal definitions.
        // ---------------------

        private void SecondaryStatByTierTable_Show()
        {
            __secondary_stat_by_tier__foldout = EditorGUILayout.Foldout(__secondary_stat_by_tier__foldout, "Secondary Stats - Tier List");

            if (__secondary_stat_by_tier__foldout)
            {
                SecondaryStatByTierTable_ShowSecondaryStatSelector();

                SecondaryStatByTierTable_Initialize();

                SecondaryStatByTierTable_ShowData();

                SecondaryStatByTierTable_ShowControls();
            }
        }

        private void SecondaryStatByTierTable_ShowSecondaryStatSelector()
        {
            EditorGUILayout.BeginHorizontal();

            DropDownList secondary_stat_dropdown = DropDownList.Create(DAL.SecondaryStatTable.GetField());

            GUIContent[] secondary_stat_names = secondary_stat_dropdown.field_name.ToArray();

            EditorGUIUtility.labelWidth = GUI.skin.label.CalcSize(new GUIContent("Secondary Stat:")).x;
            __secondary_stat_by_tier__secondary_stat_id = secondary_stat_dropdown.GetFieldId(
                EditorGUILayout.Popup(
                    new GUIContent("Secondary Stat:"),
                    secondary_stat_dropdown.GetDropdownID(__secondary_stat_by_tier__secondary_stat_id),
                    secondary_stat_names));

            EditorGUILayout.EndHorizontal();
        }

        private void SecondaryStatByTierTable_Initialize()
        {
            __secondary_stat_by_tier_table = DAL.SecondaryStatByTierTable.GetField(
                DAL.SecondaryStatByTierTable.SECONDARY_STAT_ID_COLUMN_NAME,
                __secondary_stat_by_tier__secondary_stat_id);
        }

        private void SecondaryStatByTierTable_ShowData()
        {
            for (int i = 0; i < __secondary_stat_by_tier_table.Count; i++)
            {
                EditorGUILayout.BeginHorizontal();

                POCO.Tier tier = DAL.TierTable.GetField(__secondary_stat_by_tier_table[i].tier_id);

                __secondary_stat_by_tier_table[i].value = EditorGUILayout.IntField(
                    tier.name,
                    __secondary_stat_by_tier_table[i].value);

                EditorGUILayout.EndHorizontal();
            }
        }

        private void SecondaryStatByTierTable_ShowControls()
        {
            EditorGUILayout.BeginHorizontal();

            if (GUILayout.Button("Clear Table", GUILayout.ExpandWidth(false)))
            {
                SecondaryStatByTierTable_ClearTable();
            }

            if (GUILayout.Button("Save Table", GUILayout.ExpandWidth(false)))
            {
                SecondaryStatByTierTable_SaveTable();
            }
    
            EditorGUILayout.EndHorizontal();
        }

        private void SecondaryStatByTierTable_SaveTable()
        {
            foreach (POCO.SecondaryStatByTier secondary_stat_by_tier in __secondary_stat_by_tier_table)
            {
                DAL.SecondaryStatByTierTable.SetField(secondary_stat_by_tier);
            }

            SecondaryStatByTierTable_Initialize();
        }

        private void SecondaryStatByTierTable_ClearTable()
        {
            DAL.SecondaryStatByTierTable.RemoveField();

            SecondaryStatByTierTable_Initialize();
        }

        #endregion

        #region SecondaryStatByGear Table

        // -----------------
        // Internal members.
        // -----------------

        private bool __secondary_stat_by_gear__foldout = false;
        private int __secondary_stat_by_gear__gear_type_id = 0;
        private int __secondary_stat_by_gear__gear_id = 0;
        private List<POCO.SecondaryStatByGear> __secondary_stat_by_gear_table = null;

        // ---------------------
        // Internal definitions.
        // ---------------------

        private void SecondaryStatByGearTable_Show()
        {
            __secondary_stat_by_gear__foldout = EditorGUILayout.Foldout(__secondary_stat_by_gear__foldout, "Secondary Stats - Gear List");

            if (__secondary_stat_by_gear__foldout)
            {
                SecondaryStatByGearTable_ShowSelectors();

                SecondaryStatByGearTable_Initialize();

                SecondaryStatByGearTable_ShowData();
            }
        }

        private void SecondaryStatByGearTable_ShowSelectors()
        {
            EditorGUILayout.BeginHorizontal();

            DropDownList gear_types_dropdown = DropDownList.Create(DAL.GearTypeTable.GetField());

            GUIContent[] gear_type_names = gear_types_dropdown.field_name.ToArray();

            EditorGUIUtility.labelWidth = GUI.skin.label.CalcSize(new GUIContent("Gear Type:")).x;
            __secondary_stat_by_gear__gear_type_id = gear_types_dropdown.GetFieldId(
                EditorGUILayout.Popup(
                    new GUIContent("Gear Type:"),
                    gear_types_dropdown.GetDropdownID(__secondary_stat_by_gear__gear_type_id),
                    gear_type_names));

            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();

            DropDownList gear_dropdown = DropDownList.Create(DAL.GearTable.GetField(__secondary_stat_by_gear__gear_type_id));

            GUIContent[] gear_names = gear_dropdown.field_name.ToArray();

            EditorGUIUtility.labelWidth = GUI.skin.label.CalcSize(new GUIContent("Gear:")).x;
            __secondary_stat_by_gear__gear_id = gear_dropdown.GetFieldId(
                EditorGUILayout.Popup(
                    new GUIContent("Gear:"),
                    gear_dropdown.GetDropdownID(__secondary_stat_by_gear__gear_id),
                    gear_dropdown.field_name.ToArray()));

            EditorGUILayout.EndHorizontal();
        }

        private void SecondaryStatByGearTable_Initialize()
        {
            __secondary_stat_by_gear_table = DAL.SecondaryStatByGearTable.GetField(
                __secondary_stat_by_gear__gear_id);
        }

        private void SecondaryStatByGearTable_ShowData()
        {
            Tuple<int, int[]>[] headers = new Tuple<int, int[]>[]
            {
                new Tuple<int, int[]>(1, new int[] { 3, 4, 5 }),
                new Tuple<int, int[]>(2, new int[] { 5 }),
                new Tuple<int, int[]>(3, new int[] { 9 }),
            };

            DropDownList secondary_stat_dropdown = DropDownList.Create(DAL.SecondaryStatTable.GetField());
            DropDownList tier_dropdown = DropDownList.Create(DAL.TierTable.GetField());

            GUIContent[] secondary_stat_names = secondary_stat_dropdown.field_name.ToArray();
            GUIContent[] tier_names = tier_dropdown.field_name.ToArray();

            foreach (Tuple<int, int[]> header in headers)
            {
                EditorGUIUtility.labelWidth = GUI.skin.label.CalcSize(new GUIContent(header.Item1 + " stat")).x;
                EditorGUILayout.LabelField(new GUIContent(header.Item1 + " stat"));

                foreach (int star in header.Item2)
                {
                    bool available = DAL.GearByStarsTable.GetField(__secondary_stat_by_gear__gear_id).stars <= star;

                    if (available)
                    {
                        EditorGUILayout.BeginHorizontal();

                        EditorGUIUtility.labelWidth = GUI.skin.label.CalcSize(new GUIContent(star + " stars:")).x;
                        EditorGUILayout.LabelField(star + " stars:");

                        if (GUILayout.Button("Add Field", GUILayout.ExpandWidth(false)))
                        {
                            DAL.SecondaryStatByGearTable.AddField(
                                __secondary_stat_by_gear__gear_id, 
                                star, 
                                header.Item1, 
                                1, 
                                1);
                        }

                        EditorGUILayout.EndHorizontal();

                        foreach (POCO.SecondaryStatByGear field in __secondary_stat_by_gear_table)
                        {
                            if (field.stat_line == header.Item1 && field.stars == star)
                            {
                                EditorGUILayout.BeginHorizontal();

                                EditorGUIUtility.labelWidth = GUI.skin.label.CalcSize(new GUIContent("Secondary Stat:")).x;
                                int secondary_stat_id = secondary_stat_dropdown.GetFieldId(
                                    EditorGUILayout.Popup(
                                        new GUIContent("Secondary Stat:"),
                                        secondary_stat_dropdown.GetDropdownID(field.secondary_stat_id),
                                        secondary_stat_names));

                                EditorGUIUtility.labelWidth = GUI.skin.label.CalcSize(new GUIContent("Tier:")).x;
                                int tier_id = tier_dropdown.GetFieldId(
                                    EditorGUILayout.Popup(
                                        new GUIContent("Tier:"),
                                        tier_dropdown.GetDropdownID(field.tier_id),
                                        tier_names));

                                if (field.secondary_stat_id != secondary_stat_id || field.tier_id != tier_id)
                                {
                                    field.secondary_stat_id = secondary_stat_id;
                                    field.tier_id = tier_id;

                                    DAL.SecondaryStatByGearTable.SetField(field);
                                }

                                EditorGUILayout.EndHorizontal();
                            }
                        }
                    }
                }
            }
        }

        #endregion
    }
}
