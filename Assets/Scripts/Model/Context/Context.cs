﻿using UnityEngine;
using UnityEngine.Assertions;

namespace MQB.Model
{
    public static class Context
    {
        // -------------------
        // Public definitions.
        // -------------------

        /*! \brief Return the context containing application data 
         */
        public static UnityContext GetUnityContext()
        {
            GameObject model = GameObject.FindGameObjectWithTag("Context");
            Assert.IsNotNull(model);
            return model.GetComponent<UnityContext>();
        }

        /*! \brief Return the context containing application data 
         */
        public static SqliteContext GetSqliteContext()
        {
            GameObject model = GameObject.FindGameObjectWithTag("Context");
            Assert.IsNotNull(model);
            return model.GetComponent<SqliteContext>();
        }
    }
}
