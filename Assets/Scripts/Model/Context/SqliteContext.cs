﻿using System;
using System.Data;
using System.IO;
using Mono.Data.Sqlite;
using UnityEngine;
using UnityEngine.Assertions;

namespace MQB.Model
{
    public class SqliteContext : MonoBehaviour
    {
        // -----------------
        // Internal members.
        // -----------------

        private IDbConnection __database = null;

        // ------------------
        // Public properties.
        // ------------------

        private IDbConnection database
        {
            get
            {
                return __database;
            }

            set
            {
                __database = value;
            }
        }

        // -------------------
        // Public definitions.
        // -------------------

        /*! \brief Helper method to execute a writing sql command
         * 
         * \param command Command to execute.
         */
        public void ExecuteCommand(string command)
        {
            Assert.IsNotNull(database, "Database is null");

            IDbCommand cmd = database.CreateCommand();

            cmd.CommandText = command;

            cmd.ExecuteNonQuery();
        }

        /*! \brief Helper method to execute a reading sql command
         * 
         * \param command Command to execute.
         */
        public IDataReader ExecuteReaderCommand(string command)
        {
            Assert.IsNotNull(database, "Database is null");

            IDbCommand cmd = database.CreateCommand();

            cmd.CommandText = command;

            return cmd.ExecuteReader();
        }

        // ---------------------
        // Internal definitions.
        // ---------------------

        private void Awake()
        {
            Open();
        }

        private void OnDestroy()
        {
            Close();
        }

        /*! \brief Open the database
         */
        private void Open()
        {
            string database_path = "";

#if UNITY_ANDROID && !UNITY_EDITOR

            // Set the path where database will be copied
            //
            database_path = Path.Combine(Application.persistentDataPath, "mqb.db");

            // If the database isn't already copied
            //
            if (!File.Exists(database_path))
            {
                // Decompress the database
                //
                WWW loadDB = new WWW(Path.Combine(Application.streamingAssetsPath, "mqb.db"));
 
                while(!loadDB.isDone);

                // Copy the database to its new location
                //
                File.WriteAllBytes(database_path, loadDB.bytes);
            }

#else
            database_path = Path.Combine(Application.streamingAssetsPath, "mqb.db");
#endif

            try
            {
                string connection = "Data Source = " + database_path + "; Version = 3";
                database = new SqliteConnection(connection);

                database.Open();
            }
            catch (Exception e)
            {
                Debug.LogError("Can't open database!\n\tException: " + e.Message + "\n" + "\tInner Exception: " +  e.InnerException + "\n");
            }
        }

        /*! \brief Close the database
         */
        private void Close()
        {
            database.Close();
        }
    }
}
