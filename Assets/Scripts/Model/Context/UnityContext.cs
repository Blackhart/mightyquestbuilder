﻿using UnityEngine;
using UnityEngine.Assertions;

namespace MQB.Model
{
    public class UnityContext : MonoBehaviour
    {
        // ---------------
        // Public members.
        // ---------------

        public POCO.SecondaryStatByGearMenu secondary_stat_by_gear_menu;
    }
}
