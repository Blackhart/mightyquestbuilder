﻿using System.Collections.Generic;
using System.Data;

namespace MQB.Model.DAL
{
    public static class GearByGearTypeTable
    {
        // --------------------
        // Internal Constantes.
        // --------------------

        public static readonly string TABLE_NAME = "GearByGearType";
        public static readonly string ID_COLUMN_NAME = "id";
        public static readonly string GEAR_ID_COLUMN_NAME = "gear_id";
        public static readonly string GEAR_TYPE_ID_COLUMN_NAME = "gear_type_id";

        // -------------------
        // Public definitions.
        // -------------------

        public static void AddField(int gear_id, int gear_type_id)
        {
            string command = string.Format(
                "INSERT INTO {0} ({1}, {2}) VALUES ({3}, {4})",
                TABLE_NAME,
                GEAR_ID_COLUMN_NAME,
                GEAR_TYPE_ID_COLUMN_NAME,
                gear_id,
                gear_type_id);

            Context.GetSqliteContext().ExecuteCommand(command);
        }

        public static void SetField(POCO.GearByGearType gear_by_gear_type)
        {
            string command = string.Format(
                "UPDATE {0} SET {1}={2}, {3}={4} WHERE {5}={6}",
                TABLE_NAME,
                GEAR_ID_COLUMN_NAME,
                gear_by_gear_type.gear_id,
                GEAR_TYPE_ID_COLUMN_NAME,
                gear_by_gear_type.gear_type_id,
                ID_COLUMN_NAME,
                gear_by_gear_type.id);

            Context.GetSqliteContext().ExecuteCommand(command);
        }

        public static void RemoveField()
        {
            string command = string.Format(
                "DELETE FROM {0}",
                TABLE_NAME);

            Context.GetSqliteContext().ExecuteCommand(command);
        }

        public static List<POCO.GearByGearType> GetField()
        {
            string command = string.Format(
                "SELECT * FROM {0}",
                TABLE_NAME);

            IDataReader reader = Context.GetSqliteContext().ExecuteReaderCommand(command);

            List<POCO.GearByGearType> table = new List<POCO.GearByGearType>();

            while (reader.Read())
            {
                table.Add(new POCO.GearByGearType
                {
                    id = reader.GetInt32(reader.GetOrdinal(ID_COLUMN_NAME)),
                    gear_id = reader.GetInt32(reader.GetOrdinal(GEAR_ID_COLUMN_NAME)),
                    gear_type_id = reader.GetInt32(reader.GetOrdinal(GEAR_TYPE_ID_COLUMN_NAME))
                });
            }

            return table;
        }
    }
}
