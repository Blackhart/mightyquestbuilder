﻿using System.Collections.Generic;
using System.Data;

namespace MQB.Model.DAL
{
    public static class GearByStarsTable
    {
        // --------------------
        // Internal Constantes.
        // --------------------

        private static readonly string TABLE_NAME = "GearByStars";
        private static readonly string ID_COLUMN_NAME = "id";
        private static readonly string GEAR_ID_COLUMN_NAME = "gear_id";
        private static readonly string STARS_COLUMN_NAME = "stars";

        // -------------------
        // Public definitions.
        // -------------------

        public static void AddField(int gear_id, int stars)
        {
            string command = string.Format(
                "INSERT INTO {0} ({1}, {2}) VALUES ({3}, {4})",
                TABLE_NAME,
                GEAR_ID_COLUMN_NAME,
                STARS_COLUMN_NAME,
                gear_id,
                stars);

            Context.GetSqliteContext().ExecuteCommand(command);
        }

        public static void SetField(POCO.GearByStars gear_by_stars)
        {
            string command = string.Format(
                "UPDATE {0} SET {1}={2}, {3}={4} WHERE {5}={6}",
                TABLE_NAME,
                GEAR_ID_COLUMN_NAME,
                gear_by_stars.gear_id,
                STARS_COLUMN_NAME,
                gear_by_stars.stars,
                ID_COLUMN_NAME,
                gear_by_stars.id);

            Context.GetSqliteContext().ExecuteCommand(command);
        }

        public static void RemoveField()
        {
            string command = string.Format(
                "DELETE FROM {0}",
                TABLE_NAME);

            Context.GetSqliteContext().ExecuteCommand(command);
        }

        public static List<POCO.GearByStars> GetField()
        {
            string command = string.Format(
                "SELECT * FROM {0}",
                TABLE_NAME);

            IDataReader reader = Context.GetSqliteContext().ExecuteReaderCommand(command);

            List<POCO.GearByStars> table = new List<POCO.GearByStars>();

            while (reader.Read())
            {
                table.Add(new POCO.GearByStars
                {
                    id = reader.GetInt32(reader.GetOrdinal(ID_COLUMN_NAME)),
                    gear_id = reader.GetInt32(reader.GetOrdinal(GEAR_ID_COLUMN_NAME)),
                    stars = reader.GetInt32(reader.GetOrdinal(STARS_COLUMN_NAME))
                });
            }

            return table;
        }

        public static POCO.GearByStars GetField(int gear_id)
        {
            string command = string.Format(
                "SELECT * FROM {0} WHERE {1}={2}",
                TABLE_NAME,
                GEAR_ID_COLUMN_NAME,
                gear_id);

            IDataReader reader = Context.GetSqliteContext().ExecuteReaderCommand(command);

            reader.Read();

            POCO.GearByStars field = new POCO.GearByStars
            {
                id = reader.GetInt32(reader.GetOrdinal(ID_COLUMN_NAME)),
                gear_id = reader.GetInt32(reader.GetOrdinal(GEAR_ID_COLUMN_NAME)),
                stars = reader.GetInt32(reader.GetOrdinal(STARS_COLUMN_NAME))
            };
            
            return field;
        }
    }
}
