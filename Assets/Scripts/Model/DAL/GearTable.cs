﻿using System.Collections.Generic;
using System.Data;

namespace MQB.Model.DAL
{
    public static class GearTable
    {
        // --------------------
        // Internal Constantes.
        // --------------------

        public static readonly string TABLE_NAME = "Gear";
        public static readonly string ID_COLUMN_NAME = "id";
        public static readonly string NAME_COLUMN_NAME = "name";

        // -------------------
        // Public definitions.
        // -------------------

        public static void AddField(string name)
        {
            string command = string.Format(
                "INSERT INTO {0} ({1}) VALUES (\"{2}\")",
                TABLE_NAME,
                NAME_COLUMN_NAME,
                name);

            Context.GetSqliteContext().ExecuteCommand(command);
        }

        public static void SetField(POCO.Gear gear)
        {
            string command = string.Format(
                "UPDATE {0} SET {1}=\"{2}\" WHERE {3}={4}",
                TABLE_NAME,
                NAME_COLUMN_NAME,
                gear.name,
                ID_COLUMN_NAME,
                gear.id);

            Context.GetSqliteContext().ExecuteCommand(command);
        }

        public static void RemoveField()
        {
            string command = string.Format(
                "DELETE FROM {0}",
                TABLE_NAME);

            Context.GetSqliteContext().ExecuteCommand(command);
        }

        public static List<POCO.Gear> GetField()
        {
            string command = string.Format(
                "SELECT * FROM {0}",
                TABLE_NAME);

            IDataReader reader = Context.GetSqliteContext().ExecuteReaderCommand(command);

            List<POCO.Gear> table = new List<POCO.Gear>();

            while (reader.Read())
            {
                table.Add(new POCO.Gear
                {
                    id = reader.GetInt32(reader.GetOrdinal(ID_COLUMN_NAME)),
                    name = reader.GetString(reader.GetOrdinal(NAME_COLUMN_NAME))
                });
            }

            return table;
        }

        public static List<POCO.Gear> GetField(int gear_type_id)
        {
            string command = string.Format(
                "SELECT * FROM {0} INNER JOIN {1} ON {2}={3} WHERE {4}={5}",
                TABLE_NAME,
                DAL.GearByGearTypeTable.TABLE_NAME,
                TABLE_NAME + "." + ID_COLUMN_NAME,
                DAL.GearByGearTypeTable.TABLE_NAME + "." + DAL.GearByGearTypeTable.GEAR_ID_COLUMN_NAME,
                DAL.GearByGearTypeTable.GEAR_TYPE_ID_COLUMN_NAME,
                gear_type_id);

            IDataReader reader = Context.GetSqliteContext().ExecuteReaderCommand(command);

            List<POCO.Gear> table = new List<POCO.Gear>();

            while (reader.Read())
            {
                table.Add(new POCO.Gear
                {
                    id = reader.GetInt32(reader.GetOrdinal(ID_COLUMN_NAME)),
                    name = reader.GetString(reader.GetOrdinal(NAME_COLUMN_NAME))
                });
            }

            return table;
        }
    }
}
