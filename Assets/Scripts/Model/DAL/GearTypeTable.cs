﻿using System.Data;
using System.Collections.Generic;

namespace MQB.Model.DAL
{
    public static class GearTypeTable
    {
        // --------------------
        // Internal Constantes.
        // --------------------

        private static readonly string TABLE_NAME = "GearType";
        private static readonly string ID_COLUMN_NAME = "id";
        private static readonly string NAME_COLUMN_NAME = "name";

        // -------------------
        // Public definitions.
        // -------------------

        public static void AddField(string name)
        {
            string command = string.Format(
                "INSERT INTO {0} (name) VALUES (\"{1}\")", 
                TABLE_NAME, 
                name);

            Context.GetSqliteContext().ExecuteCommand(command);
        }

        public static void SetField(POCO.GearType gear_type)
        {
            string command = string.Format(
                "UPDATE {0} SET {1}=\"{2}\" WHERE {3}={4}", 
                TABLE_NAME, 
                NAME_COLUMN_NAME, 
                gear_type.name, 
                ID_COLUMN_NAME, 
                gear_type.id);

            Context.GetSqliteContext().ExecuteCommand(command);
        }

        public static void RemoveField()
        {
            string command = string.Format(
                "DELETE FROM {0}", 
                TABLE_NAME);

            Context.GetSqliteContext().ExecuteCommand(command);
        }

        public static List<POCO.GearType> GetField()
        {
            string command = string.Format(
                "SELECT * FROM {0}", 
                TABLE_NAME);

            IDataReader reader = Context.GetSqliteContext().ExecuteReaderCommand(command);

            List<POCO.GearType> table = new List<POCO.GearType>();

            while (reader.Read())
            {
                table.Add(new POCO.GearType
                {
                    id = reader.GetInt32(reader.GetOrdinal(ID_COLUMN_NAME)),
                    name = reader.GetString(reader.GetOrdinal(NAME_COLUMN_NAME))
                });
            }

            return table;
        }
    }
}
