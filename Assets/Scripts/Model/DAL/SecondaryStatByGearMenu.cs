﻿namespace MQB.Model.DAL
{
    public static class SecondaryStatByGearMenu
    {
        // --------------
        // Public events.
        // --------------

        public static Event.UnityEvent.UnityIntEvent OnSelectedGearTypeChanged = new Event.UnityEvent.UnityIntEvent();
        public static Event.UnityEvent.UnityIntEvent OnSelectedGearChanged = new Event.UnityEvent.UnityIntEvent();

        // -------------------
        // Public definitions.
        // -------------------

        public static void SetSelectedGearType(int gear_type_id)
        {
            Context.GetUnityContext().secondary_stat_by_gear_menu.gear_type_id = gear_type_id;

            OnSelectedGearTypeChanged.Invoke(gear_type_id);
        }

        public static void SetSelectedGear(int gear_id)
        {
            Context.GetUnityContext().secondary_stat_by_gear_menu.gear_id = gear_id;

            OnSelectedGearChanged.Invoke(gear_id);
        }
    }
}
