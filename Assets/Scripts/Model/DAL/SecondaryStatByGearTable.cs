﻿using System.Collections.Generic;
using System.Data;

namespace MQB.Model.DAL
{
    public static class SecondaryStatByGearTable
    {
        // --------------------
        // Internal Constantes.
        // --------------------

        private static readonly string TABLE_NAME = "SecondaryStatByGear";
        private static readonly string ID_COLUMN_NAME = "id";
        private static readonly string GEAR_ID_COLUMN_NAME = "gear_id";
        private static readonly string STARS_COLUMN_NAME = "stars";
        private static readonly string STAT_LINE_COLUMN_NAME = "stat_line";
        private static readonly string SECONDARY_STAT_ID_COLUMN_NAME = "secondary_stat_id";
        private static readonly string TIER_ID_COLUMN_NAME = "tier_id";

        // -------------------
        // Public definitions.
        // -------------------

        public static void AddField(int gear_id, int stars, int stat_line, int secondary_stat_id, int tier_id)
        {
            string command = string.Format(
                "INSERT INTO {0} ({1}, {2}, {3}, {4}, {5}) VALUES ({6}, {7}, {8}, {9}, {10})",
                TABLE_NAME,
                GEAR_ID_COLUMN_NAME,
                STARS_COLUMN_NAME,
                STAT_LINE_COLUMN_NAME,
                SECONDARY_STAT_ID_COLUMN_NAME,
                TIER_ID_COLUMN_NAME,
                gear_id,
                stars,
                stat_line,
                secondary_stat_id,
                tier_id);

            Context.GetSqliteContext().ExecuteCommand(command);
        }

        public static void SetField(POCO.SecondaryStatByGear secondary_stat_by_gear)
        {
            string command = string.Format(
                "UPDATE {0} SET {1}={2}, {3}={4}, {5}={6}, {7}={8}, {9}={10} WHERE {11}={12}",
                TABLE_NAME,
                GEAR_ID_COLUMN_NAME,
                secondary_stat_by_gear.gear_id,
                STARS_COLUMN_NAME,
                secondary_stat_by_gear.stars,
                STAT_LINE_COLUMN_NAME,
                secondary_stat_by_gear.stat_line,
                SECONDARY_STAT_ID_COLUMN_NAME,
                secondary_stat_by_gear.secondary_stat_id,
                TIER_ID_COLUMN_NAME,
                secondary_stat_by_gear.tier_id,
                ID_COLUMN_NAME,
                secondary_stat_by_gear.id);

            Context.GetSqliteContext().ExecuteCommand(command);
        }

        public static void RemoveField()
        {
            string command = string.Format(
                "DELETE FROM {0}",
                TABLE_NAME);

            Context.GetSqliteContext().ExecuteCommand(command);
        }

        public static List<POCO.SecondaryStatByGear> GetField(int gear_id)
        {
            string command = string.Format(
                "SELECT * FROM {0} WHERE {1}={2}",
                TABLE_NAME,
                GEAR_ID_COLUMN_NAME,
                gear_id);

            IDataReader reader = Context.GetSqliteContext().ExecuteReaderCommand(command);

            List<POCO.SecondaryStatByGear> table = new List<POCO.SecondaryStatByGear>();

            while (reader.Read())
            {
                table.Add(new POCO.SecondaryStatByGear
                {
                    id = reader.GetInt32(reader.GetOrdinal(ID_COLUMN_NAME)),
                    gear_id = reader.GetInt32(reader.GetOrdinal(GEAR_ID_COLUMN_NAME)),
                    stars = reader.GetInt32(reader.GetOrdinal(STARS_COLUMN_NAME)),
                    stat_line = reader.GetInt32(reader.GetOrdinal(STAT_LINE_COLUMN_NAME)),
                    secondary_stat_id = reader.GetInt32(reader.GetOrdinal(SECONDARY_STAT_ID_COLUMN_NAME)),
                    tier_id = reader.GetInt32(reader.GetOrdinal(TIER_ID_COLUMN_NAME))
                });
            }

            return table;
        }

        public static List<POCO.SecondaryStatByGear> GetField(int gear_id, int stars, int stat_line)
        {
            string command = string.Format(
                "SELECT * FROM {0} WHERE {1}={2} AND {3}={4} AND {5}={6}",
                TABLE_NAME,
                GEAR_ID_COLUMN_NAME,
                gear_id,
                STARS_COLUMN_NAME,
                stars,
                STAT_LINE_COLUMN_NAME,
                stat_line);

            IDataReader reader = Context.GetSqliteContext().ExecuteReaderCommand(command);

            List<POCO.SecondaryStatByGear> table = new List<POCO.SecondaryStatByGear>();

            while (reader.Read())
            {
                table.Add(new POCO.SecondaryStatByGear
                {
                    id = reader.GetInt32(reader.GetOrdinal(ID_COLUMN_NAME)),
                    gear_id = reader.GetInt32(reader.GetOrdinal(GEAR_ID_COLUMN_NAME)),
                    stars = reader.GetInt32(reader.GetOrdinal(STARS_COLUMN_NAME)),
                    stat_line = reader.GetInt32(reader.GetOrdinal(STAT_LINE_COLUMN_NAME)),
                    secondary_stat_id = reader.GetInt32(reader.GetOrdinal(SECONDARY_STAT_ID_COLUMN_NAME)),
                    tier_id = reader.GetInt32(reader.GetOrdinal(TIER_ID_COLUMN_NAME))
                });
            }

            return table;
        }
    }
}
