﻿using System.Collections.Generic;
using System.Data;

namespace MQB.Model.DAL
{
    public static class SecondaryStatByTierTable
    {
        // ------------------
        // Public Constantes.
        // ------------------

        public static readonly string TABLE_NAME = "SecondaryStatByTier";
        public static readonly string ID_COLUMN_NAME = "id";
        public static readonly string SECONDARY_STAT_ID_COLUMN_NAME = "secondary_stat_id";
        public static readonly string TIER_ID_COLUMN_NAME = "tier_id";
        public static readonly string VALUE_COLUMN_NAME = "value";

        // -------------------
        // Public definitions.
        // -------------------

        public static void AddField(int secondary_stat_id, int tier_id, int value)
        {
            string command = string.Format(
                "INSERT INTO {0} ({1}, {2}, {3}) VALUES ({4}, {5}, {6})",
                TABLE_NAME,
                SECONDARY_STAT_ID_COLUMN_NAME,
                TIER_ID_COLUMN_NAME,
                VALUE_COLUMN_NAME,
                secondary_stat_id,
                tier_id,
                value);

            Context.GetSqliteContext().ExecuteCommand(command);
        }

        public static void SetField(POCO.SecondaryStatByTier secondary_stat_by_tier)
        {
            string command = string.Format(
                "UPDATE {0} SET {1}={2}, {3}={4}, {5}={6} WHERE {7}={8}",
                TABLE_NAME,
                SECONDARY_STAT_ID_COLUMN_NAME,
                secondary_stat_by_tier.secondary_stat_id,
                TIER_ID_COLUMN_NAME,
                secondary_stat_by_tier.tier_id,
                VALUE_COLUMN_NAME,
                secondary_stat_by_tier.value,
                ID_COLUMN_NAME,
                secondary_stat_by_tier.id);

            Context.GetSqliteContext().ExecuteCommand(command);
        }

        public static void RemoveField()
        {
            string command = string.Format(
                "DELETE FROM {0}",
                TABLE_NAME);

            Context.GetSqliteContext().ExecuteCommand(command);
        }

        public static List<POCO.SecondaryStatByTier> GetField()
        {
            string command = string.Format(
                "SELECT * FROM {0}",
                TABLE_NAME);

            IDataReader reader = Context.GetSqliteContext().ExecuteReaderCommand(command);

            List<POCO.SecondaryStatByTier> table = new List<POCO.SecondaryStatByTier>();

            while (reader.Read())
            {
                table.Add(new POCO.SecondaryStatByTier
                {
                    id = reader.GetInt32(reader.GetOrdinal(ID_COLUMN_NAME)),
                    secondary_stat_id = reader.GetInt32(reader.GetOrdinal(SECONDARY_STAT_ID_COLUMN_NAME)),
                    tier_id = reader.GetInt32(reader.GetOrdinal(TIER_ID_COLUMN_NAME)),
                    value = reader.GetInt32(reader.GetOrdinal(VALUE_COLUMN_NAME))
                });
            }

            return table;
        }

        public static List<POCO.SecondaryStatByTier> GetField(string column_name, int value)
        {
            string command = string.Format(
                "SELECT * FROM {0} WHERE {1}={2}",
                TABLE_NAME,
                column_name,
                value);

            IDataReader reader = Context.GetSqliteContext().ExecuteReaderCommand(command);

            List<POCO.SecondaryStatByTier> table = new List<POCO.SecondaryStatByTier>();

            while (reader.Read())
            {
                table.Add(new POCO.SecondaryStatByTier
                {
                    id = reader.GetInt32(reader.GetOrdinal(ID_COLUMN_NAME)),
                    secondary_stat_id = reader.GetInt32(reader.GetOrdinal(SECONDARY_STAT_ID_COLUMN_NAME)),
                    tier_id = reader.GetInt32(reader.GetOrdinal(TIER_ID_COLUMN_NAME)),
                    value = reader.GetInt32(reader.GetOrdinal(VALUE_COLUMN_NAME))
                });
            }

            return table;
        }

        public static List<POCO.SecondaryStatByTier> GetField(string column_name_1, int value_1, string column_name_2, int value_2)
        {
            string command = string.Format(
                "SELECT * FROM {0} WHERE {1}={2} AND {3}={4}",
                TABLE_NAME,
                column_name_1,
                value_1,
                column_name_2,
                value_2);

            IDataReader reader = Context.GetSqliteContext().ExecuteReaderCommand(command);

            List<POCO.SecondaryStatByTier> table = new List<POCO.SecondaryStatByTier>();

            while (reader.Read())
            {
                table.Add(new POCO.SecondaryStatByTier
                {
                    id = reader.GetInt32(reader.GetOrdinal(ID_COLUMN_NAME)),
                    secondary_stat_id = reader.GetInt32(reader.GetOrdinal(SECONDARY_STAT_ID_COLUMN_NAME)),
                    tier_id = reader.GetInt32(reader.GetOrdinal(TIER_ID_COLUMN_NAME)),
                    value = reader.GetInt32(reader.GetOrdinal(VALUE_COLUMN_NAME))
                });
            }

            return table;
        }
    }
}
