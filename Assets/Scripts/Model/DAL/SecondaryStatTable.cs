﻿using System.Data;
using System.Collections.Generic;

namespace MQB.Model.DAL
{
    public static class SecondaryStatTable
    {
        // --------------------
        // Internal Constantes.
        // --------------------

        private static readonly string TABLE_NAME = "SecondaryStat";
        private static readonly string ID_COLUMN_NAME = "id";
        private static readonly string NAME_COLUMN_NAME = "name";

        // -------------------
        // Public definitions.
        // -------------------

        public static void AddField(string name)
        {
            string command = string.Format(
                "INSERT INTO {0} ({1}) VALUES (\"{2}\")",
                TABLE_NAME,
                NAME_COLUMN_NAME,
                name);

            Context.GetSqliteContext().ExecuteCommand(command);
        }

        public static void SetField(POCO.SecondaryStat secondary_stat)
        {
            string command = string.Format(
                "UPDATE {0} SET {1}=\"{2}\" WHERE {3}={4}",
                TABLE_NAME,
                NAME_COLUMN_NAME,
                secondary_stat.name,
                ID_COLUMN_NAME,
                secondary_stat.id);

            Context.GetSqliteContext().ExecuteCommand(command);
        }

        public static void RemoveField()
        {
            string command = string.Format(
                "DELETE FROM {0}",
                TABLE_NAME);

            Context.GetSqliteContext().ExecuteCommand(command);
        }

        public static List<POCO.SecondaryStat> GetField()
        {
            string command = string.Format(
                "SELECT * FROM {0}",
                TABLE_NAME);

            IDataReader reader = Context.GetSqliteContext().ExecuteReaderCommand(command);

            List<POCO.SecondaryStat> table = new List<POCO.SecondaryStat>();

            while (reader.Read())
            {
                table.Add(new POCO.SecondaryStat
                {
                    id = reader.GetInt32(reader.GetOrdinal(ID_COLUMN_NAME)),
                    name = reader.GetString(reader.GetOrdinal(NAME_COLUMN_NAME))
                });
            }

            return table;
        }

        public static POCO.SecondaryStat GetField(int field_id)
        {
            string command = string.Format(
                "SELECT {0} FROM {1} WHERE {2}={3}",
                NAME_COLUMN_NAME,
                TABLE_NAME,
                ID_COLUMN_NAME,
                field_id);

            IDataReader reader = Context.GetSqliteContext().ExecuteReaderCommand(command);

            reader.Read();

            POCO.SecondaryStat field = new POCO.SecondaryStat
            {
                id = field_id,
                name = reader.GetString(reader.GetOrdinal(NAME_COLUMN_NAME))
            };

            return field;
        }
    }
}
