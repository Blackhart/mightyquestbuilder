﻿using System.Collections.Generic;
using System.Data;

namespace MQB.Model.DAL
{
    public static class TierTable
    {
        // --------------------
        // Internal Constantes.
        // --------------------

        private static readonly string TABLE_NAME = "Tier";
        private static readonly string ID_COLUMN_NAME = "id";
        private static readonly string NAME_COLUMN_NAME = "name";

        // -------------------
        // Public definitions.
        // -------------------

        public static void AddField(string name)
        {
            string command = string.Format(
                "INSERT INTO {0} ({1}) VALUES (\"{2}\")",
                TABLE_NAME,
                NAME_COLUMN_NAME,
                name);

            Context.GetSqliteContext().ExecuteCommand(command);
        }

        public static void SetField(POCO.Tier tier)
        {
            string command = string.Format(
                "UPDATE {0} SET {1}=\"{2}\" WHERE {3}={4}",
                TABLE_NAME,
                NAME_COLUMN_NAME,
                tier.name,
                ID_COLUMN_NAME,
                tier.id);

            Context.GetSqliteContext().ExecuteCommand(command);
        }

        public static void RemoveField()
        {
            string command = string.Format(
                "DELETE FROM {0}",
                TABLE_NAME);

            Context.GetSqliteContext().ExecuteCommand(command);
        }

        public static List<POCO.Tier> GetField()
        {
            string command = string.Format(
                "SELECT * FROM {0}",
                TABLE_NAME);

            IDataReader reader = Context.GetSqliteContext().ExecuteReaderCommand(command);

            List<POCO.Tier> table = new List<POCO.Tier>();

            while (reader.Read())
            {
                table.Add(new POCO.Tier
                {
                    id = reader.GetInt32(reader.GetOrdinal(ID_COLUMN_NAME)),
                    name = reader.GetString(reader.GetOrdinal(NAME_COLUMN_NAME))
                });
            }

            return table;
        }

        public static POCO.Tier GetField(int field_id)
        {
            string command = string.Format(
                "SELECT * FROM {0} WHERE {1}={2}",
                TABLE_NAME,
                ID_COLUMN_NAME,
                field_id);

            IDataReader reader = Context.GetSqliteContext().ExecuteReaderCommand(command);

            reader.Read();

            POCO.Tier field = new POCO.Tier
            {
                id = reader.GetInt32(reader.GetOrdinal(ID_COLUMN_NAME)),
                name = reader.GetString(reader.GetOrdinal(NAME_COLUMN_NAME))
            };

            return field;
        }
    }
}
