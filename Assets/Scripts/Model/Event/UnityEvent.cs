﻿using UnityEngine.Events;

namespace MQB.Model.Event
{
    public class UnityEvent
    {
        // --------------
        // Public events.
        // --------------

        public class UnityIntEvent : UnityEvent<int> { }
    }
}
