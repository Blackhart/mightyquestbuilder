﻿namespace MQB.Model.POCO
{
    [System.Serializable]
    public class Gear
    {
        /*! \brief id */
        public int id;

        /*! \brief Gear name */
        public string name;
    }
}
