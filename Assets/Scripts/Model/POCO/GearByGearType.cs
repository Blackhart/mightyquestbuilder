﻿namespace MQB.Model.POCO
{
    public class GearByGearType
    {
        /*! \brief Field ID */
        public int id;

        /*! \brief Gear ID */
        public int gear_id;

        /*! \brief Gear type ID */
        public int gear_type_id;
    }
}
