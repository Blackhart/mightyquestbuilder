﻿namespace MQB.Model.POCO
{
    public class GearByStars
    {
        /*! \brief Field id */
        public int id;

        /*! \brief Gear id */
        public int gear_id;

        /*! \brief Number of stars at which the gear is available */
        public int stars;
    }
}
