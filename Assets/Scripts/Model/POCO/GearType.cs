﻿namespace MQB.Model.POCO
{
    public class GearType
    {
        /*! \brief Gear type id */
        public int id;

        /*! \brief Gear type name */
        public string name;
    }
}
