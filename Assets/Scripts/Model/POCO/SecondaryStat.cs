﻿namespace MQB.Model.POCO
{
    [System.Serializable]
    public class SecondaryStat
    {
        /*! \brief Field ID */
        public int id;

        /*! \brief Secondary stat name */
        public string name;
    }
}
