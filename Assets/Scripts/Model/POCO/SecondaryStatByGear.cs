﻿namespace MQB.Model.POCO
{
    [System.Serializable]
    public class SecondaryStatByGear
    {
        /*! \brief Field ID */
        public int id;

        /*! \brief Gear ID */
        public int gear_id;

        /*! \brief Stars number at which the stat roll */
        public int stars;

        /*! \brief Stat line at which the stat roll */
        public int stat_line;

        /*! \brief Secondary stat ID */
        public int secondary_stat_id;

        /*! \brief Tier ID */
        public int tier_id;
    }
}
