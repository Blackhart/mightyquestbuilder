﻿namespace MQB.Model.POCO
{
    [System.Serializable]
    public class SecondaryStatByGearMenu
    {
        /*! \brief Selected gear type ID */ 
        public int gear_type_id;

        /*! \brief Selected gear ID */
        public int gear_id;
    }
}
