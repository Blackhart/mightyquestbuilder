﻿namespace MQB.Model.POCO
{
    [System.Serializable]
    public class SecondaryStatByTier
    {
        /*! \brief Field id */
        public int id;

        /*! \brief Secondary stat ID */
        public int secondary_stat_id;

        /*! \brief Tier ID */
        public int tier_id;

        /*! \brief Value */
        public int value;
    }
}
