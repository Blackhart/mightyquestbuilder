﻿namespace MQB.Model.POCO
{
    [System.Serializable]
    public class Tier
    {
        /*! \brief field id */
        public int id;

        /*! \brief Tier name */
        public string name;
    }
}
