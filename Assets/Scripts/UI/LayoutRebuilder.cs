﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LayoutRebuilder : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        // Due to Unity UI issue, we have to rebuild the secondary stat list to be draw properly
        //
        UnityEngine.UI.LayoutRebuilder.ForceRebuildLayoutImmediate(GetComponent<RectTransform>());
    }

    // Update is called once per frame
    void LateUpdate()
    {
        // Due to Unity UI issue, we have to rebuild the secondary stat list to be draw properly
        //
        UnityEngine.UI.LayoutRebuilder.ForceRebuildLayoutImmediate(GetComponent<RectTransform>());
    }
}
