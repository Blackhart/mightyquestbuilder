﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace MQB.Model.View
{
    public class GearSelectorView : MonoBehaviour
    {
        // -----------------
        // Internal members.
        // -----------------

        /*! \brief List of all gears */
        private List<POCO.Gear> __gears = null;

        // -------------------
        // Public definitions.
        // -------------------

        /*! \brief Signal called when the user select a gear in the dropdown list
         * 
         * \param gear_dropdown_id Dropdown ID of the selected gear.
         */
        public void OnGearSelected_Signal(int gear_dropdown_id)
        {
            DAL.SecondaryStatByGearMenu.SetSelectedGear(__gears[gear_dropdown_id].id);
        }

        // ---------------------
        // Internal definitions.
        // ---------------------

        private void Awake()
        {
            InitializeSignals();
        }

        /*! \brief Initialize signals
         */
        private void InitializeSignals()
        {
            DAL.SecondaryStatByGearMenu.OnSelectedGearTypeChanged.AddListener(InitializeDropDown);
        }

        /*! \brief Initialize the dropdown list
         */
        private void InitializeDropDown(int gear_type_id)
        {
            // Get the list of gears
            //
            __gears = DAL.GearTable.GetField(gear_type_id);

            // Populate the dropdown list with all gear names
            //
            Dropdown dropdown = GetComponent<Dropdown>();

            dropdown.options.Clear();

            foreach (POCO.Gear gear in __gears)
            {
                dropdown.options.Add(new Dropdown.OptionData(gear.name));
            }

            dropdown.value = 0;
            
            dropdown.RefreshShownValue();

            // Call signal once to populate the list of secondary stats
            //
            OnGearSelected_Signal(0);

        }
    }
}
