﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace MQB.Model.View
{
    public class GearTypeSelectorView : MonoBehaviour
    {
        // -----------------
        // Internal members.
        // -----------------

        /*! \brief List of all gear types */
        private List<POCO.GearType> __gear_types = null;

        // -------------------
        // Public definitions.
        // -------------------

        /*! \brief Signal called when the user select a gear type in the dropdown list
         * 
         * \param gear_type_dropdown_id Dropdown ID of the selected gear type.
         */
        public void OnGearTypeSelected_Signal(int gear_type_dropdown_id)
        {
            DAL.SecondaryStatByGearMenu.SetSelectedGearType(__gear_types[gear_type_dropdown_id].id);
        }

        // ---------------------
        // Internal definitions.
        // ---------------------

        private void Start()
        {
            InitializeDropDown();
        }

        /*! \brief Initialize the dropdown list
         */
        private void InitializeDropDown()
        {
            // Get the list of gear types
            //
            __gear_types = DAL.GearTypeTable.GetField();

            // Populate the dropdown list with all gear type names
            //
            Dropdown dropdown = GetComponent<Dropdown>();

            foreach (POCO.GearType gear in __gear_types)
            {
                dropdown.options.Add(new Dropdown.OptionData(gear.name));
            }

            dropdown.RefreshShownValue();

            // Call signal once to populate the dropdown list of gears
            //
            OnGearTypeSelected_Signal(0);
        }
    }
}
