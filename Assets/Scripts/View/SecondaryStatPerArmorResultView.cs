﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace MQB.Model.View
{
    public class SecondaryStatPerArmorResultView : MonoBehaviour
    {
        // -----------
        // Constantes.
        // -----------

        private static readonly string ELEMENT_TEXT = "<color=#386B94>Increase</color> <color=#1B8BCA>{0}</color> <color=#386B94>by</color> <color=#1B8BCA>{1}%</color><color=#386B94>.</color>";

        // ---------------
        // Public members.
        // ---------------

        /*! \brief Stat line 1 / Stars 3 secondary stats UI elements
         */
        public GameObject l1s3;
        public List<GameObject> l1s3_elements;

        /*! \brief Stat line 1 / Stars 4 secondary stats UI elements
         */
        public GameObject l1s4;
        public List<GameObject> l1s4_elements;

        /*! \brief Stat line 1 / Stars 5 secondary stats UI elements
         */
        public GameObject l1s5;
        public List<GameObject> l1s5_elements;

        /*! \brief Stat line 2 / Stars 5 secondary stats UI elements
         */
        public GameObject l2s5;
        public List<GameObject> l2s5_elements;

        /*! \brief Stat line 3 / Stars 9 secondary stats UI elements
         */
        public GameObject l3s9;
        public List<GameObject> l3s9_elements;

        // -----------------
        // Internal members.
        // -----------------

        /*! \brief Selected gear ID
         */
        private int __gear_id;

        // ---------------------
        // Internal definitions.
        // ---------------------

        private void Awake()
        {
            InitializeSignals();
        }

        /*! \brief Initialize signals
         */
        private void InitializeSignals()
        {
            DAL.SecondaryStatByGearMenu.OnSelectedGearChanged.AddListener(OnSelectedGearChanged_Event);
        }

        /*! \brief Called when the user select a new gear in the dropdown list
         */
        private void OnSelectedGearChanged_Event(int gear_id)
        {
            __gear_id = gear_id;

            UpdateResult();
        }

        /*! \brief Draw all secondary stats for the selected gear
         */
        private void UpdateResult()
        {
            UpdateFirstLineStats();
            UpdateSecondLineStats();
            UpdateThirdLineStats();
        }

        /*! \brief Draw all first stat line secondary stats for the selected gear
         */
        private void UpdateFirstLineStats()
        {
            int star = DAL.GearByStarsTable.GetField(__gear_id).stars;

            List<POCO.SecondaryStatByGear> l1s3_stats = DAL.SecondaryStatByGearTable.GetField(
                __gear_id,
                3,
                1);

            List<POCO.SecondaryStatByGear> l1s4_stats = DAL.SecondaryStatByGearTable.GetField(
                __gear_id,
                4,
                1);

            List<POCO.SecondaryStatByGear> l1s5_stats = DAL.SecondaryStatByGearTable.GetField(
                __gear_id,
                5,
                1);

            // Hide stat line 1 view if the selected gear registered any value for that stat line
            //
            l1s3.SetActive(star <= 3 && l1s3_stats.Count > 0);
            l1s4.SetActive(star <= 4 && l1s4_stats.Count > 0);
            l1s5.SetActive(l1s5_stats.Count > 0);

            UpdateStatElements(l1s3_elements, l1s3_stats);
            UpdateStatElements(l1s4_elements, l1s4_stats);
            UpdateStatElements(l1s5_elements, l1s5_stats);
        }

        /*! \brief Draw all second stat line secondary stats for the selected gear
         */
        private void UpdateSecondLineStats()
        {
            List<POCO.SecondaryStatByGear> l2s5_stats = DAL.SecondaryStatByGearTable.GetField(
                __gear_id,
                5,
                2);

            // Hide stat line 2 view if the selected gear registered any value for that stat line
            //
            l2s5.SetActive(l2s5_stats.Count > 0);

            UpdateStatElements(l2s5_elements, l2s5_stats);
        }

        /*! \brief Draw all third stat line secondary stats for the selected gear
         */
        private void UpdateThirdLineStats()
        {
            List<POCO.SecondaryStatByGear> l3s9_stats = DAL.SecondaryStatByGearTable.GetField(
                __gear_id,
                9,
                3);

            // Hide stat line 3 view if the selected gear registered any value for that stat line
            //
            l3s9.SetActive(l3s9_stats.Count > 0);

            UpdateStatElements(l3s9_elements, l3s9_stats);
        }

        /*! \brief Helper method to draw a secondary stat line
         */
        private void UpdateStatElements(List<GameObject> list_elements, List<POCO.SecondaryStatByGear> stats)
        {
            // Iterate over all secondary stats for the selected gear
            //
            for (int i = 0; i < list_elements.Count; i++)
            {
                // We have four pre-allocated secondary stat line in the UI list
                // So, we have to check if the number of registered secondary stat for the selected gear 
                // falls under the number of pre-allocated secondary stat lines
                //
                if (i < stats.Count)
                {
                    // If the pre-allocated secondary stat line is used,
                    // We show it and populate it with secondary stat data of the selected gear
                    //
                    list_elements[i].SetActive(true);

                    list_elements[i].GetComponentInChildren<Text>().text = string.Format(
                        ELEMENT_TEXT,
                        DAL.SecondaryStatTable.GetField(stats[i].secondary_stat_id).name,
                        DAL.SecondaryStatByTierTable.GetField(
                            DAL.SecondaryStatByTierTable.SECONDARY_STAT_ID_COLUMN_NAME,
                            stats[i].secondary_stat_id,
                            DAL.SecondaryStatByTierTable.TIER_ID_COLUMN_NAME,
                            stats[i].tier_id)[0].value);
                }
                else
                {
                    // If the pre-allocated secondary stat line isn't used,
                    // we hide it.
                    //
                    list_elements[i].SetActive(false);

                    list_elements[i].GetComponentInChildren<Text>().text = string.Format(ELEMENT_TEXT, "???", "???");
                }
            }
        }
    }
}
